/* Service for the control and management of electronic passes*/


const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const objectId = require('mongodb').ObjectID;
//const inputVisitor = require('./sysRead');


const app = express();
let db;

function addDate(inputData){

    inputData.date = new Date();
    let day = inputData.date.getDate();
    let month = inputData.date.getMonth();
    let year = inputData.date.getFullYear();
    let hours = inputData.date.getHours();
    let minute = inputData.date.getMinutes();

    inputData.date = `${day} ${month+1} ${year} ${hours}:${minute}`;
    
    return inputData;
}

function checkDbVisitor(inputData){
    const collection = app.locals.collectionVisitors;
    if(inputData){
        collection.findOne({"_id": objectId(inputData.id)})
        .then((visitor)=>{
            visitor.status = true;
            console.log(visitor);
        }).catch(err=>{console.log(err)});   
    }
    else{
        console.log('Не зарегистрированный посетитель');
        db.close();
    }
}

function createJournalDateVisit(inputData){    
    app.locals.collectionVisitors.findOne({"_id": objectId(inputData.id)})
    .then((result)=>{ 
          return result;
    })
    .then((result)=>{
        const journal = {date_arrive: inputData.date, exit_time : "", owner_id : result._id};
        const collectionJournals = app.locals.collectionVisitJournal;
        collectionJournals.insertOne(journal,(err,resultJournal)=>{
            if(resultJournal){
                return resultJournal;
             }
            else{
                console.log(err);
            }
        })
    })
}

function correctExit(exitData){
    app.locals.collectionVisitors.findOne({"_id": objectId(exitData.id)})
    .then((result)=>{ 
        result.status = false;
        return result;
    });
}

function updateJournalDateVisit(exitData){
    app.locals.collectionVisitJournal.findOne({"owner_id": objectId(exitData.id)},{sort:{$natural:-1}})
    .then(res=>{
        res.exit_time = exitData.date;
        console.log(res._id);
        return res;
     }).then(res=>{
        app.locals.collectionVisitJournal.updateOne({"_id": res._id},{$set:{"exit_time" : res.exit_time}})
        .then((result)=>{
            return result;
        })
     })
}
    
MongoClient.connect('mongodb://localhost:27017/dima_collection',{useUnifiedTopology : true})
.then((database)=>{
    db = database.db("dima_collection");
    app.locals.collectionVisitors = db.collection("visitors");
    app.locals.collectionVisitJournal = db.collection("visitJournal");
    
    app.listen(3000,()=>{
        console.log("app started");
    });

}).catch(()=>{console.log("Error of connect")});

app.get('/visitorsList',(req,res)=>{
    const collection = req.app.locals.collectionVisitors;
    collection.find({}).toArray((err,visitor)=>{
        console.log(visitor);
        res.send(visitor)
    })
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.post('/exit',(req,res)=>{
    if(!req.body) return res.status(400);

    const exitVisitor = req.body;

    correctExit(exitVisitor);
    updateJournalDateVisit(addDate(exitVisitor));
    res.send(exitVisitor);
});

app.post('/entrance',(req,res)=>{

    if(!req.body) return res.status(400);
    const inputVisitor = req.body;

    checkDbVisitor(inputVisitor);
    addDate(inputVisitor);
    createJournalDateVisit(inputVisitor);
    res.send(inputVisitor);
});




